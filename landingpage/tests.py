from django.test import TestCase, Client
from django.urls import resolve
from landingpage.views import landingpage
from django.http import HttpRequest
from landingpage.views import landingpage, redirecting
from landingpage.models import StatusModel
from landingpage.forms import StatusForm
from selenium import webdriver
import unittest
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


# Create your tests here.
class NewVisitorTest(TestCase):

    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)
        super(NewVisitorTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(NewVisitorTest, self).tearDown()

    def test_can_start_a_list_and_retrieve_it_later(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/landingpage/')
        message = selenium.find_element_by_id('id_message')
        submit = selenium.find_element_by_id('button')
        message.send_keys('Coba Coba')
        submit.send_keys(Keys.RETURN)
        time.sleep(2)
        self.assertIn("Coba Coba", selenium.page_source)


    
class UnitTestStory6(TestCase):
    def test_landingpage_url_is_exist(self):
        response = Client().get('/landingpage/')
        self.assertEqual(response.status_code, 200)

    def test_notexist_url_is_notexist(self):
        response = Client().get('/bla/')
        self.assertEqual(response.status_code, 404)

    def test_landingpage_using_landingpage_function(self):
        response = resolve('/landingpage/')
        self.assertEqual(response.func, landingpage)
    
    def test_landingpage_using_landingpage_template(self):
        response = Client().get('/landingpage/')
        self.assertTemplateUsed(response, 'landingpage.html')
    
    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = landingpage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Halo, apa kabar?', html_response)

    def test_landing_page_title_is_right(self):
        request = HttpRequest()
        response = landingpage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<title>Story 6</title>', html_response)

    def test_landing_page_using_redirecting_function(self):
        response = resolve('/')
        self.assertEqual(response.func, redirecting)
    
    def test_landing_page_redirecting(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)

    def test_landing_page_redirected_to_home(self):
        response = Client().get('/')
        self.assertRedirects(response, '/landingpage/')
    
    def setUp(cls):
        StatusModel.objects.create(message="test1")
        
    def test_if_models_in_database(self):
        modelsObject = StatusModel.objects.create(message="test2")
        num_Object = StatusModel.objects.all().count()
        self.assertEqual(num_Object, 2)
    
    def test_if_StatusModel_status_is_exist(self):
        modelsObject = StatusModel.objects.get(id=1)
        objectstatus = StatusModel._meta.get_field('message').verbose_name
        self.assertEqual(objectstatus, 'message')
    
    def test_forms_input_html(self):
        form = StatusForm()
        self.assertIn('id="id_message', form.as_p())
        
    def test_forms_validation_blank(self):
        form = StatusForm(data={'message':''})
        self.assertFalse(form.is_valid())
        self.assertEquals(form.errors['message'], ["This field is required."])

    def test_forms_in_template(self):
        request = HttpRequest()
        response = landingpage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form method="POST" class="register_form">', html_response)


if __name__ == '__main__':#
   unittest.main(warnings='ignore')