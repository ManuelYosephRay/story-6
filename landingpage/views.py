from django.shortcuts import render
from django.shortcuts import render, redirect
from landingpage.models import StatusModel
from landingpage.forms import StatusForm


# Create your views here.

def landingpage(request):
	if request.method == "POST":
		form = StatusForm(request.POST)
		if form.is_valid():
			data = form.save()
			data.save()
			return redirect('/landingpage/')
	else:
		form = StatusForm()
	objekstatus = StatusModel.objects.order_by('-date')
	return render(request, 'landingpage.html', {'form': form, 'message': objekstatus})

def redirecting(request):
	return redirect('/landingpage/')


