from django.contrib import admin
from django.urls import path
from . import views

app_name = "landingpage"

urlpatterns = [
    path('landingpage/', views.landingpage, name="landingpage"),
    path('', views.redirecting, name='redirecting'),
]